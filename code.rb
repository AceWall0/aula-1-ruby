# Questão 1 ===============================
def sumAndMul(vector)
    sum = 0
    mul = 1
    for arr in vector do
        for value in arr do
            sum += value
            mul *= value
        end
    end
    puts sum, mul
end
test = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
sumAndMul(test)


# Questão 2 ===============================
def divide(a, b)
    return a.to_f / b.to_f
end
#test
puts divide(5, 2)


# Questão 3
def toSquare(dict)
    arr = []
    dict.each_value { |v| 
        arr.push(v ** 2)
    }
    return arr
end
test = {:chave1 => 5, :chave2 => 30, :chave3 => 20}
puts toSquare(test)


# Questão 4 ===============================
def intArrayToString(arr)
    result = []
    for value in arr do
        result.push(value.to_s)
    end
    return result
end
test = [25, 35, 45]
print intArrayToString(test)


# Questão 5 ===============================
def filterByDivder(arr, div)
    newArr = []
    for value in arr do
        if value % div == 0
            newArr.push(value)
        end
    end
    return newArr
end
test = [3, 6, 7, 8]
print filterByDivder(test, 3)
